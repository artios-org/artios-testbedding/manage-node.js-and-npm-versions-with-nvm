# Manage Node.js and npm versions with nvm

This guide will setup nvm in order to manage multiple Node.js and npm versions.

## Relative works

- _None at the moment._

## Prerequisites

- [nvm](https://github.com/nvm-sh/nvm#installing-and-updating) must be installed.

## Guide

_Highly inspired by the [Downloading and installing Node.js and npm - docs.npmjs.com](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) guide._

### Setup the environment

nvm allows to install and manage multiple Node.js (and npm) versions. At the time of writing this guide, the latest Long Term Support version (LTS) is Node.js v16.x.x.

Use the following commands to install and set a Node.js version.

```sh
# Visualize all available versions
nvm ls-remote

# Install a Node.js version (latest LTS version)
nvm install v16

# Set the Node.js version for the current terminal
nvm use v16

# Set the Node.js version globally
nvm alias default v16

# Display the Node.js and npm versions
node --version
npm --version
```

You can now switch between multiple Node.js versions depending on the project's requirements.

## Additional resources

- _None at the moment._

## License

This work is licensed under the [Non-Profit Open Software License version 3.0](https://opensource.org/licenses/NPOSL-3.0).
